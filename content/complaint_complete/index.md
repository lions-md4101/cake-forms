---
title: Christmas Cake Complaint Form Submission
draft: false
---

Thank you for your Lions Christmas Cakes complaint form. Your complaint has been forwarded to the District Christmas Cakes chair, who will be in touch as soon as possible. 
