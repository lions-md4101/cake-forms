class HTML(object):
    def __init__(
        self,
        destination,
        district_description,
        cake_chair_name,
        cake_chair_email,
    ):
        self.out = [
            "---",
            f"title: {district_description} Christmas Cakes Order Form",
            "draft: false",
            "---",
            "",
            '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>',
            '<script src="/js/reg_form.js"></script>',
            "<h3>Orders are required by 31 July</h2>",
            "<h4><u>Kindly note</u>: Deliveries will be made to central locations, not to individual clubs. You will be contacted to confirm the central location you wish to use for your delivery.</h4>"
            "<h4>Christmas Cakes will be made available on a first come first served basis. In other words, cakes will be made available according to the date the order was received. Cakes will be delivered to your chosen central location as close to your requested delivery date as possible.</h4>",
            f'<h4>Clubs are responsible for following up on their cakes order. Clubs must also be aware of the requirements that were placed with their order.<br>To confirm your order, you can contact your District Chair for Christmas Cakes:<br>{cake_chair_name} - Email: <a href="mailto:{cake_chair_email}">{cake_chair_email}</a></h4>',
            f'<form name="{destination}" method="POST" data-netlify="true" action="/order-complete">',
        ]
        self.level = 1
        self.out.append(f"<h3>{district_description} Christmas Cakes Order Form</h3>")

    def close(self):
        self.out.append(f'{"~" * self.level}<center>')
        self.level += 1
        self.out.append(
            f'{"~" * self.level}<button type="submit">Submit Order Form</button>'
        )
        self.level -= 1
        self.out.append(f'{"~" * self.level}</center>')
        self.level -= 1
        self.out.append("<h3>Payment terms:</h3>")
        self.out.append(f'{"~" * self.level}<ul>')
        self.level += 1
        self.out.append(
            f'{"~" * self.level}<li>Payment for Christmas cakes must be made within 60 days from the invoice, or by 31 December, whichever is sooner. In other words, If the invoice is dated 6 September, the amount owed must be paid by 5 November. If the invoice is dated 1 December, it must be paid by 31 December.</li>'
        )
        self.out.append(
            f'{"~" * self.level}<li>Invoices will be emailed to the 2 email addresses supplied by the Club on the order form.</li>'
        )
        self.out.append(
            f'{"~" * self.level}<li><b>To qualify for the R5.00 per cake settlement rebate, all invoices must be settled within 35 days from the invoice, or before 31 December, whichever is sooner</b>. In other words, If the invoice is dated 6 September, the amount owed must be paid on or before 10 November. The settlement rebate will be paid into your account by the District Treasurer during the first week of February. In order to qualify for the settlement rebate, payment must reflect in the baker’s account before the due date. Payments made on the due date will not qualify.</li>'
        )

        self.level -= 1
        self.out.append(f'{"~" * self.level}</ul>')

        self.out.append("<h3>Complaint Procedure:</h3>")
        self.out.append(f'{"~" * self.level}<ul>')
        self.level += 1
        self.out.append(
            f'{"~" * self.level}<li>Complainant completes the complaint form. Minimum information required:</li>'
        )
        self.out.append(f'{"~" * self.level}<ul>')
        self.level += 1
        self.out.append(
            f'{"~" * self.level}<li>Batch number and production date - printed on inner packaging cellophane.</li>'
        )
        self.out.append(
            f'{"~" * self.level}<li>Photographs of problem i.e. mould, damaged cartons, damaged cakes.</li>'
        )
        self.out.append(f'{"~" * self.level}<li>Number of cakes affected.</li>')
        self.out.append(
            f'{"~" * self.level}<li>Complaint form and photographs sent to District Chairperson.</li>'
        )
        self.out.append(
            f'{"~" * self.level}<li>District Chairpersons record complaints and forwards to MD Cake Chairperson.</li>'
        )
        self.out.append(
            f'{"~" * self.level}<li>"No complaint form" = no complaint.</li>'
        )
        self.level -= 1
        self.out.append(f'{"~" * self.level}</ul>')
        self.level -= 1
        self.out.append(f'{"~" * self.level}</ul>')

        self.out.append("</form>")

    def render(self):
        return "\n".join([l.replace("~", "   ") for l in self.out])

    def open_containing_div(self, cls=None):
        if cls:
            cls_div = f' id="{cls}_div"'
            cls_fs = f' id="{cls}_fs"'
        else:
            cls_div = ""
            cls_fs = ""
        self.out.append(f"{'~' * self.level}<div{cls_div}>")
        self.level += 1
        self.out.append(f"{'~' * self.level}<fieldset{cls_fs} disabled>")
        self.level += 1

    def close_containing_div(self):
        self.level -= 1
        self.out.append(f"{'~' * self.level}</fieldset>")
        self.level -= 1
        self.out.append(f"{'~' * self.level}</div>")

    def open_form_item(self, tag, label, number=False):
        self.out.append(f'{"~" * self.level}<div class="form-group row">')
        self.level += 1
        self.out.append(
            f'{"~" * self.level}<label for="{tag}" class="col-sm-{"8" if number else "4"} col-form-label">'
        )
        self.level += 1
        self.out.append(f'{"~" * self.level}{label}: ')
        self.level -= 1
        self.out.extend(
            (
                f'{"~" * self.level}</label>',
                f'{"~" * self.level}<div class="col-sm-{"4" if number else "8"}">',
            )
        )
        self.level += 1

    def close_form_item(self):
        self.level -= 1
        self.out.append(f'{"~" * self.level}</div>')
        self.level -= 1
        self.out.append(f'{"~" * self.level}</div>')

    def add_header(self, text):
        self.out.append(f'{"~" * self.level}<h2>{text}</h2>')

    def add_label(self, tag, text, centre=False, font=None):
        self.out.append(f'{"~" * self.level}<div class="form-group row">')
        self.level += 1
        if centre:
            self.out.append(f'{"~" * self.level}<center>')
            self.level += 1
        self.out.append(
            f'{"~" * self.level}<label id={tag}>{"<h3>" if font=="b" else ""}{text}{"</h3>" if font=="b" else ""}</label>'
        )
        self.level -= 1
        if centre:
            self.out.append(f'{"~" * self.level}</center>')
            self.level -= 1
        self.out.append(f'{"~" * self.level}</div>')

    def add_text(
        self,
        tag,
        label,
        help="",
        type="text",
        cls="",
        cost=None,
        disabled=False,
        required=True,
    ):
        if label:
            self.open_form_item(tag, label, number=type == "number")
        if help:
            help_attr = f' aria-describedby="{tag}_help"'
        else:
            help_attr = ""
        if cost:
            cost_attr = f" cost={cost}"
        else:
            cost_attr = ""
        base_class = "form-control"
        if cls:
            class_attr = f"{base_class} {cls}"
        else:
            class_attr = base_class
        m = ""
        if type == "number":
            m = 'min="1" value="1"'
        inner = [
            f'{"~" * self.level}<input type="{type}" {m} {"required" if required else ""} class="{class_attr}" id="{tag}" name="{tag}"{help_attr}{cost_attr}{" disabled" if disabled else ""}>'
        ]
        if help:
            inner.append(
                f'{"~" * self.level}<small id="{tag}_help" class="form-text text-muted">'
            )
            self.level += 1
            inner.append(f'{"~" * self.level}{help}')
            self.level -= 1
            inner.append(f'{"~" * self.level}</small>')
        self.out.extend(inner)
        if label:
            self.close_form_item()

    def add_email(self, tag, label, help="", required=True):
        self.add_text(tag, label, help=help, type="email", required=required)

    def add_checkbox(self, tag, label, help="", required=True):
        if help:
            help_attr = f' aria-describedby="{tag}_help"'
        else:
            help_attr = ""
        self.out.append(f'{"~" * self.level}<div class="form-check">')
        self.level += 1
        self.out.extend(
            (
                f'{"~" * self.level}<input {"required" if required else ""} class="form-check-input" type="checkbox" value="{tag}" name="{tag}" id="{tag}"{help_attr}>',
                f'{"~" * self.level}<label class="form-check-label" for="{tag}">',
            )
        )
        self.level += 1
        self.out.append(f'{"~" * self.level}{label}')
        self.level -= 1
        self.out.append(f'{"~" * self.level}</label>')
        if help:
            self.out.append(
                f'{"~" * self.level}<small id="{tag}_help" class="form-text text-muted">'
            )
            self.level += 1
            self.out.append(f'{"~" * self.level}{help}')
            self.level -= 1
            self.out.append(f'{"~" * self.level}</small>')
        self.level -= 1
        self.out.append(f'{"~" * self.level}</div>')

    def add_selector(self, tag, label, items, help="", dummy_text="", required=False):
        """If dummy_text, add the text as the first entry"""
        self.open_form_item(tag, label)
        if help:
            help_attr = f' aria-describedby="{tag}_help"'
        else:
            help_attr = ""
        inner = [
            f'{"~" * self.level}<select class="form-control" id="{tag}" name="{tag}"{help_attr}{" required" if required else ""}>'
        ]
        self.level += 1
        if dummy_text:
            inner.append(f'{"~" * self.level}<option value="">{dummy_text}</option>')
        inner.extend(
            [
                f'{"~" * self.level}<option value="{item}">{item}</option>'
                for item in items
            ]
        )
        self.level -= 1
        inner.append(f'{"~" * self.level}</select>')
        if help:
            inner.append(
                f'{"~" * self.level}<small id="{tag}_help" class="form-text text-muted">'
            )
            self.level += 1
            inner.append(f'{"~" * self.level}{help}')
            self.level -= 1
            inner.append(f'{"~" * self.level}</small>')
        self.out.extend(inner)
        self.close_form_item()

    def add_radios(self, name, options):
        self.out.append(f'{"~" * self.level}<div class="form-group row">')
        self.level += 1
        for (n, (k, v)) in enumerate(options.items()):
            self.out.append(f'{"~" * self.level}<div class="form-check">')
            self.level += 1
            self.out.append(
                f'{"~" * self.level}<input class="form-check-input" type="radio" name="{name}" id="{k}" value="{k}"{" checked" if n == 0 else ""}>'
            )
            self.out.append(
                f'{"~" * self.level}<label class="form-check-label" for="{k}">'
            )
            self.level += 1
            self.out.append(f'{"~" * self.level}{v}')
            self.level -= 1
            self.out.append(f'{"~" * self.level}</label>')
            self.level -= 1
            self.out.append(f'{"~" * self.level}</div>')
        self.level -= 1
        self.out.append(f'{"~" * self.level}</div>')

    def add_divider(self):
        self.out.append(f'{"~" * self.level}<hr>')


for (destination, district, chair_name, chair_email) in (
    ("410e", "District 410E", "Teresa Panteleon", "teresap@vodamail.co.za"),
    ("410w", "District 410W", "Jeannie Van Wulven", "cakeorder410w@gmail.com"),
):
    html = HTML(destination, district, chair_name, chair_email)
    html.add_text("date_required", "Date Cakes are Required")
    html.add_text("club", "Club Name")
    html.add_text(
        "cases",
        "Number of Cases required (X12 Cakes)",
        type="number",
    )
    html.add_text("responsible_member_name", "Responsible Member: Name and Surname")
    html.add_email("responsible_member_email", "Responsible Member: Email Address")
    html.add_text("responsible_member_cell", "Responsible Member: Cell Number")
    html.add_text("president_name", "Club President: Name and Surname")
    html.add_email("president_email", "Club President: Email Address")
    html.add_text("president_cell", "Club President: Cell Number")
    html.add_checkbox(
        "payment_terms",
        "I have read through and acknowledge the payment terms (see below form)",
    )
    html.add_checkbox(
        "complaints",
        "I have read through and acknowledge the complaints procedure (see below form)",
    )
    html.add_text("comments_requests", "Any further comments/requests", required=False)
    html.close()

    with open(f"../../content/order/{destination}/_index.html", "w") as fh:
        fh.write(html.render())
    print(html.render())
